
import java.io.*;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;

public class Requete {

    static final String file1  = "Endangered_species.ttl";
    static final String file2  = "Ntriples_Lombaerde_Cosson.ttl";
    static final String file3  = "pollution.ttl";

    public static void main (String args[]) {
    	Model model = ModelFactory.createDefaultModel();

        InputStream in = FileManager.get().open( file1 );
        if (in == null) {
            throw new IllegalArgumentException( "File: " + file1 + " not found");
        }

        model.read(in, null, "Turtle");

        in = FileManager.get().open( file2 );
        if (in == null) {
        	throw new IllegalArgumentException( "File: " + file2 + " not found");
        }

        model.read(in, null, "Turtle");

        in = FileManager.get().open( file3 );
        if (in == null) {
        	throw new IllegalArgumentException( "File: " + file3 + " not found");
        }

        model.read(in, null, "Turtle");

        String prefix = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
        		"PREFIX dbo: <http://dbpedia.org/ontology/>\n" +
        		"PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
        		"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
        		"PREFIX wdws: <http://www.wikidata.org/wiki/Special:EntityData/>\n" +
        		"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
        		"PREFIX ex: <http://www.ex.org/>" +
        		"PREFIX exa: <http://ex.org/a#>" +
        		"PREFIX dbpedia-owl: <http://dbpedia.org/ontology/location>" +
        		"PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>" +
        		"PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#Temperature>";

        String queryString1 = prefix + "select ?commonName\n"
        		+ "where {\n"
        		+ "?x dbo:family ?family;\n"
        		+ "   wdws:P225 ?commonName;\n"
        		+ "   wdws:P141 ?status.\n"
        		+ "?family ex:group ?group.\n"
        		+ "?group rdfs:label ?groupName.\n"
        		+ "filter(?groupName = \"Mammals\").\n"
        		+ "filter(?status = \"Endangered\")\n"
        		+ "} limit 100";

    	Query query1 = QueryFactory.create(queryString1);
    	try (QueryExecution qexec = QueryExecutionFactory.create(query1, model)) {
    		ResultSet results = qexec.execSelect() ;
    		ResultSetFormatter.out(System.out, results, query1);
	    }

    	String queryString2 = prefix + "select ?familyName (count(?scientificName) as ?nbEndangered)\n"
        		+ "where {\n"
        		+ "?x rdfs:label ?scientificName;\n"
        		+ "   wdws:P141 ?status;\n"
        		+ "   dbo:family ?family.\n"
        		+ "?family rdfs:label ?familyName;\n"
        		+ "        ex:group ?group.\n"
        		+ "?group ex:type ?type.\n"
        		+ "?type rdfs:label ?typeName.\n"
        		+ "filter(?typeName = \"Plant\").\n"
        		+ "filter(?status = \"Endangered\").\n"
        		+ "} group by ?familyName\n"
        		+ "order by desc(?nbEndangered)";
    	Query query2 = QueryFactory.create(queryString2);
    	try (QueryExecution qexec = QueryExecutionFactory.create(query2, model)) {
    		ResultSet results = qexec.execSelect() ;
    		ResultSetFormatter.out(System.out, results, query2);
	    }

    	String queryString3 = prefix + "select ?country ?nbEndangered ?avgTemp ?avgcNO2\n"
        		+ "where {\n"
        		+ "		{\n"
        		+ "			select ?country (count(?scientificName) as ?nbEndangered)"
        		+ "			where{\n"
        		+ "				?x  dbo:country ?country;\n"
        		+ "					rdfs:label ?scientificName;\n"
        		+ "   				wdws:P141 \"Endangered\".\n"
        		+ "			}group by ?country\n"
        		+ "		}\n"
        		+ "		{\n"
    			  + "			select ?country (avg(?temperature) as ?avgTemp)\n"
        		+ "			where {\n"
        		+ "				?y dbpedia-owl:locationCountry ?country;\n"
        		+ "					dbpedia-owl:locationCity ?city.\n"
        		+ "				{\n"
        		+ "					select ?city (max(?cityDate) as ?maxDate)\n"
        		+ "					where{\n"
        		+ "						?z dbpedia-owl:locationCountry ?country;\n"
        		+ "							dbpedia-owl:locationCity ?city;\n"
        		+ "							crm:E50_Date ?cityDate;\n"
        		+ "					}group by ?city\n"
        		+ "				}\n"
        		+ "				?y crm:E50_Date ?maxDate;\n"
        		+ "					hto:Temperature ?temperature.\n"
        		+ "			}group by ?country\n"
        		+ "		}\n"
        		+ "		{\n"
        		+ "			select ?country (avg(?cNO2) as ?avgcNO2)\n"
        		+ "			where {\n"
    			  + "				?a dbo:city ?ICity.\n"
    			  + "				?ICity dbo:country ?ICountry.\n"
    			  + "				?ICountry exa:valueCountry ?country.\n"
    			  + "				{\n"
    			  + "					select ?ICity (max(?measureDate) as ?maxMeasureDate)\n"
    			  + "					where{\n"
    			  + "						?b dbo:city ?ICity;\n"
    			  + "							exa:DateMeasure ?measureDate.\n"
    			  + "					}group by ?ICity\n"
    			  + "				}\n"
    			  + "				?a exa:DateMeasure ?maxMeasureDate;\n"
    			  + "					exa:concentrationNO2 ?INO2.\n"
    			  + "				?INO2 exa:valueNO2 ?cNO2.\n"
        		+ "			} group by ?country\n"
        		+ "		}\n"
        		+ "}order by desc(?nbEndangered)\n";
    	Query query3 = QueryFactory.create(queryString3);
    	try (QueryExecution qexec = QueryExecutionFactory.create(query3, model)) {
    		ResultSet results = qexec.execSelect();
    		ResultSetFormatter.out(System.out, results, query3);
	    }

    }
}
