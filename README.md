Le fichier Endangere_species.ttl est le dataset que nous proposons,

généré à partir du fichier datasetWebSemantic?csv et du constructeur mapping.sparql.

Le fichier Requete.java contient trois requête dont la troisième récupère des informations aux travers de deux autres dataset,

pollution.ttl de Clement et Mathis, et

Ntriples_Lombaerde_Cosson.ttl de Julien et Alissa.
